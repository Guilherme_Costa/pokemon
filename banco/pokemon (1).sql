-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 01-Jul-2019 às 22:07
-- Versão do servidor: 10.3.15-MariaDB
-- versão do PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `pokemon`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente`
--

CREATE TABLE `cliente` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `senha` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `data` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `cliente`
--

INSERT INTO `cliente` (`id`, `nome`, `email`, `senha`, `status`, `data`) VALUES
(1, 'admin', 'admin@suporte.com', '21232f297a57a5a743894a0e4a801fc3', 1, '2019-07-01 21:59:10'),
(2, 'guilherme', 'g@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b', 1, '2019-07-01 22:02:04'),
(3, 'João', 'j@j.com', 'c4ca4238a0b923820dcc509a6f75849b', 0, '2019-07-01 22:03:26');

-- --------------------------------------------------------

--
-- Estrutura da tabela `comentario`
--

CREATE TABLE `comentario` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_publicacao` int(11) NOT NULL,
  `conteudo` varchar(255) NOT NULL,
  `data` datetime NOT NULL,
  `curtida` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `comentario`
--

INSERT INTO `comentario` (`id`, `id_cliente`, `id_publicacao`, `conteudo`, `data`, `curtida`) VALUES
(1, 2, 1, ' nam phasellus rhoncus', '2019-07-01 22:02:15', NULL),
(2, 3, 2, ' sapien primis rhoncus\r\n', '2019-07-01 22:03:41', NULL),
(3, 3, 1, ' sapien primis rhoncus\r\n', '2019-07-01 22:03:46', NULL),
(4, 2, 3, ' sapien primis rhoncu', '2019-07-01 22:05:12', NULL),
(5, 2, 2, ' elit est potenti morbi. eget s', '2019-07-01 22:05:20', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `noticia`
--

CREATE TABLE `noticia` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `conteudo` varchar(500) NOT NULL,
  `data` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `noticia`
--

INSERT INTO `noticia` (`id`, `id_cliente`, `titulo`, `conteudo`, `data`) VALUES
(1, 1, 'Lorem ipsum suscipit per', 'Dollicitudin pharetra quisque cursus urna, ornare sapien consectetur congue cursus ut inceptos venenatis aenean, posuere convallis eu feugiat facilisis magna venenatis. ullamcorper nulla aliquet molestie dui interdum, tristique mauris himenaeos porttitor volutpat, blandit mattis', '2019-07-01'),
(2, 1, 'velit quam vestibulum', 'Elutpat, non ut mauris sociosqu libero porttitor. quis enim augue aptent tortor aenean per ornare vulputate pulvinar ante, sed etiam curabitur at nostra ultrices aliquam himenaeos. lorem placerat nulla facilisis auctor lobortis per bibendum fringilla, erat euismod diam vestibulum sodales consequat velit volutpat, potenti molestie magna et aenean hendrerit etiam. ', '2019-07-01'),
(3, 1, 'Egestas sagittis gravida pulvinar', ' pulvinar praesent sed pharetra morbi, habitasse curae erat potenti amet tortor, ultrices cubilia sit nullam tristique lacinia. laoreet vehicula fusce interdum pretium tempus sem mi volutpat, quis senectus suscipit iaculis mi auctor in ad nunc, mollis sapien semper pellentesque volutpat curae dolor. quis elementum aliquam posuere mollis commodo habitasse nam phasellus', '2019-07-01'),
(4, 1, ' rhoncus mauris ullamcorpe', ' primis a vestibulum nisl enim ligula scelerisque bibendum cursus lacinia. suspendisse magna sed et mattis curabitur ante lacus porttitor, sollicitudin eleifend condimentum adipiscing lectus curabitur pellentesque conubia, vestibulum magna hendrerit cubilia et iaculis dictum. ', '2019-07-01');

-- --------------------------------------------------------

--
-- Estrutura da tabela `publicacao`
--

CREATE TABLE `publicacao` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `conteudo` varchar(500) NOT NULL,
  `data` datetime NOT NULL,
  `curtida` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `publicacao`
--

INSERT INTO `publicacao` (`id`, `id_cliente`, `titulo`, `conteudo`, `data`, `curtida`) VALUES
(1, 1, 'Lorem ipsum suscipit per', ' primis a vestibulum nisl enim ligula scelerisque bibendum cursus lacinia. suspendisse magna sed et mattis curabitur ante lacus porttitor, sollicitudin eleifend condimentum adipiscing lectus curabitur pellentesque conubia, vestibulum magna hendrerit cubilia et iaculis dictum. ', '2019-07-01 22:01:29', NULL),
(2, 2, ' porta massa elit massa', 'Integer justo elit et odio torquent etiam vulputate, odio morbi aliquam tempus suscipit commodo, vehicula porta massa elit massa imperdiet. tincidunt sit bibendum in per ac feugiat aliquam blandit sapien primis rhoncus ad ante, nullam ut primis ante lobortis conubia egestas eu elit est potenti morbi. eget senectus ac magna urna curabitur ad maecenas, ligula nibh lorem malesuada nostra', '2019-07-01 22:02:50', NULL),
(3, 3, 'entum, non egesta', 'nteger justo elit et odio torquent etiam vulputate, odio morbi aliquam tempus suscipit commodo, vehicula porta massa elit massa imperdiet. tincidunt sit bibendum in per ac feugiat aliquam blandit sapien primis rhoncus ad ante, nullam ut primis ante lobortis conubia egestas eu elit est potenti morbi. eget senectus ac magna urna curabitur ad maecenas, ligula nibh lorem malesuada Inostra elementum, non egestas iaculis dapibus felis auctor. habitasse tortor morbi mi lorem turpis cursus etiam primis', '2019-07-01 22:04:18', NULL),
(4, 2, 'assa imperdiet uam bland', 'nteger justo elit et odio torquent etiam vulputate, odio morbi aliquam tempus suscipit commodo, vehicula porta massa elit massa imperdiet. tincidunt sit bibendum in per ac feugiat aliquam blandit sapien primis rhoncus ad ante, nullam ut primis ante lobortis conubia egestas eu elit est potenti morbi. eget senectus ac magna urna curabitur ad maecenas, ligula nibh lorem malesuada Inostra elementum, non egestas iaculis dapibus felis auctor. habitasse tortor morbi mi lorem turpis cursus etiam primis', '2019-07-01 22:04:59', NULL);

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `comentario`
--
ALTER TABLE `comentario`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `noticia`
--
ALTER TABLE `noticia`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `publicacao`
--
ALTER TABLE `publicacao`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `comentario`
--
ALTER TABLE `comentario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de tabela `noticia`
--
ALTER TABLE `noticia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de tabela `publicacao`
--
ALTER TABLE `publicacao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
