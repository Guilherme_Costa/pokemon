$(document).ready(function () {
    $("#formLogin").submit(function (e) {
        e.preventDefault(); 
        $.ajax({
            type: 'POST',
            url: '/login',
            data: $("#formLogin").serializeArray(),

            success: function (json) {
                $("#div_erro").html(json);
				setTimeout(function () {
                    $("#div_erro").css({display: "none"});
                }, 3000);
            },
            error: function () {
                $("#div_erro").html("Erro em chamar a função.");
                setTimeout(function () {
                    $("#div_erro").css({display: "none"});
                }, 5000);
            }

        });
    });
});