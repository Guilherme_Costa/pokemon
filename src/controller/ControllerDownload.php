<?php

namespace POKEMON\controller;

use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

class ControllerDownload{
    private $response;
    private $twig;
    
    function __construct(Response $response, Environment $twig) {
        $this->response = $response;
        $this->twig = $twig;
    }

    public function show(){
        return $this->response->setContent($this->twig->render('download.html'));
    }
}
