<?php

namespace POKEMON\controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Twig\Environment;
use POKEMON\util\Sessao;
use POKEMON\model\Publicacao;
use POKEMON\model\Comentario;
use POKEMON\model\Cliente;
use POKEMON\entity\Publicacoes;
use POKEMON\entity\Comentarios;
use POKEMON\entity\Clientes;

class ControllerCommunity{
    private $response;
    private $twig;
    private $contexto;
    private $sessao;
    
    function __construct(Response $response, Environment $twig,  Sessao $sessao, Request $contexto) {
        $this->response = $response;
        $this->twig = $twig;
        $this->contexto = $contexto;
        $this->sessao = $sessao;
    }

    public function show(){
        //return $this->response->setContent($this->twig->render('community.html'));
        $modelo_p = new Publicacao();
        $publicacoes = $modelo_p->listarPublicacao();
        $modelo_c = new Comentario();
        $comentarios = $modelo_c->listarComentario();
        $modelo_u = new Cliente();
        $clientes = $modelo_u->listarCliente();
        $id_user = $this->sessao->get('id');
        return $this->response->setContent($this->twig->render('community.html', ['publicacoes' => $publicacoes, 
        'comentarios' => $comentarios, 'clientes' => $clientes, 'id_user' => $id_user]));

    }

    public function publicar(){

        $publicacao = new Publicacoes();
        $publicacao->setTitulo($this->contexto->get('titulo'));
        $publicacao->setConteudo(nl2br($this->contexto->get('conteudo')));
        $publicacao->setId_c($this->sessao->get('id'));
        $publicacao->setData(date('Y-m-d H:i:s'));

        $modelo = new Publicacao();
        $modelo->inserir($publicacao);
        $redirect = new RedirectResponse('/community');
        $redirect->send();
    }

    public function comentar(){

        $comentario = new Comentarios();
        $comentario->setConteudo($this->contexto->get('comentario'));
        $comentario->setId_c($this->sessao->get('id'));
        $comentario->setId_p($this->contexto->get('id_p'));
        $comentario->setData(date('Y-m-d H:i:s'));

        $modelo = new Comentario();
        $modelo->inserir($comentario);
        $redirect = new RedirectResponse('/community');
        $redirect->send();

    }

    public function delete_p(){

        $id_post = $this->contexto->get('del_p');

        $modelo_p = new Publicacao();
        $modelo_p->delete($id_post);
        
        $modelo_c = new Comentario();
        $modelo_c->delete_post($id_post);

        $redirect = new RedirectResponse('/community');
        $redirect->send();

    }

    public function alterar(){

        $id_post = $this->contexto->get('alt_p');
        print_r($id_post);
        /*$modelo = new Publicacao();
        $modelo->delete($id_post);

        $redirect = new RedirectResponse('/community');
        $redirect->send();*/

    }

    public function delete_n(){

        $id_post = $this->contexto->get('del_p');

        $modelo_p = new Publicacao();
        $modelo_p->delete_n($id_post);

        $redirect = new RedirectResponse('/');
        $redirect->send();

    }
    
}