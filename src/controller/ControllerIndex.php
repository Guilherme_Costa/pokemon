<?php

namespace POKEMON\controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Twig\Environment;
use POKEMON\util\Sessao;
use POKEMON\entity\Publicacoes;
use POKEMON\model\Publicacao;
use POKEMON\model\Cliente;

class ControllerIndex{
    private $response;
    private $twig;
    private $sessao;

    function __construct(Response $response, Environment $twig, Sessao $sessao,  Request $contexto) {
        $this->response = $response;
        $this->twig = $twig;
        $this->sessao = $sessao;
        $this->contexto = $contexto;
    }

    public function show(){
        $modelo_p = new Publicacao();
        $publicacoes = $modelo_p->listarNoticia();
        $modelo_u = new Cliente();
        $adm = $modelo_u->retorna_adm();
        //if($this->sessao->get('login') == '')
        return $this->response->setContent($this->twig->render('index.html', ['publicacoes' => $publicacoes, 
        'adm' => $adm]));
    }

    public function erro(){
        return $this->response->setContent($this->twig->render('erro.html'));
    }

    public function noticia(){
        $noticia = new Publicacoes();
        $noticia->setTitulo($this->contexto->get('titulo'));
        $noticia->setConteudo($this->contexto->get('conteudo'));
        $noticia->setId_c($this->sessao->get('id'));
        $noticia->setData(date('Y-m-d H:i:s'));

        $modelo = new Publicacao();
        $modelo->inserir_n($noticia);
        $redirect = new RedirectResponse('/');
        $redirect->send();
    }
}