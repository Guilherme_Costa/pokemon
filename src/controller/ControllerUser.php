<?php

namespace POKEMON\controller;

use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use POKEMON\model\Cliente;
use Symfony\Component\HttpFoundation\RedirectResponse;
use POKEMON\entity\Clientes;
use Symfony\Component\HttpFoundation\Request;
use POKEMON\util\Sessao;

class ControllerUser{
    private $response;
    private $twig;
    private $contexto;
    private $sessao;
    
    function __construct(Response $response, Environment $twig,  Sessao $sessao, Request $contexto) {
        $this->response = $response;
        $this->twig = $twig;
        $this->contexto = $contexto;
        $this->sessao = $sessao;
    }

    public function show(){
        if($this->sessao->existe('login') == false){
            return $this->response->setContent($this->twig->render('user.html'));
        }
        else{
            //print_r($this->sessao->get('id'));
            $redirect = new RedirectResponse('/');
            $redirect->send();
            //return $this->response->setContent($this->twig->render('erro.html'));
        }
    }

    public function cadastrar(){

        $nome = $this->contexto->get('r-nome');
        $email = $this->contexto->get('r-email');
        $senha = $this->contexto->get('r-senha');
        $confirmarSenha = $this->contexto->get('confirmarSenha');
        $data = date('Y-m-d H:i:s');

        $modelo = new Cliente();

        if(!($modelo->verificar_c($email, $nome))){
            if ($senha == $confirmarSenha){

                $cliente = new Clientes();
                $cliente->setNome($nome);
                $cliente->setEmail($email);

                $senha = md5($senha);

                $cliente->setSenha($senha);

                $cliente->setData($data);
            
                $modelo->inserir($cliente);
                $id = $modelo->retorna_id($nome, $senha);

                $this->sessao->add('id', $id);
                $this->sessao->add('login', $nome);

                echo '
                <script language="javascript" type="text/javascript">
                    javascript:history.back();
                </script>';

            }
            else {
                /*echo '
                <script>
                    alert("Senhas não estão iguais!");
                </script>';*/
                echo 'senha';
            }
        }
        else{
            /*echo '
            <script>
                alert("Usuario existente!");
            </script>';*/

            echo 'email';
        }
    }

    public function entrar(){

        $nome = addslashes($this->contexto->get('l-usuario'));
        $senha = $this->contexto->get('l-senha');
        $senha = md5($senha);

        $modelo = new Cliente();

        if(($modelo->verificar_l($nome, $senha))){

            $id = $modelo->retorna_id($nome, $senha);

            $this->sessao->add('id', $id);
            $this->sessao->add('login', $nome);

           echo '
            <script language="javascript" type="text/javascript">
                javascript:history.back();
            </script>';
        }
        else{

            echo 'não existe';
        }
    }

    public function sair(){
        $this->sessao->rem('login');
        $this->sessao->rem('id');
        $this->sessao->del();
        echo '
        <script language="javascript" type="text/javascript">
            javascript:history.back();
        </script>';
    }

    public function excluir(){
        $id = $this->sessao->get('id');
        $nome = $this->sessao->get('login');
        $modelo = new Cliente();
        $modelo->excluir($id, $nome);

        $this->sair();
    }
}