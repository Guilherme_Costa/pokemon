<?php

namespace POKEMON;

require_once __DIR__.'/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use POKEMON\controller\ControllerIndex;
use Symfony\Component\HttpFoundation\Response;
use Twig\Loader\FilesystemLoader;
use Twig\Environment;
use POKEMON\util\Sessao;

include 'rotas.php';

$sessao = new Sessao();
$sessao->start();

$loader = new FilesystemLoader('../src/view/');
$twig = new Environment($loader);
$twig->addGlobal("sessao", $sessao);

$response = new Response();

$request = Request::createFromGlobals();
$contexto = new RequestContext();
$contexto->fromRequest($request);

$mather = new UrlMatcher($rotas, $contexto);

try{
    $configRota = $mather->match($contexto->getPathInfo());

    $controlador = $configRota['controlador'];
    $objeto = new $controlador($response, $twig, $sessao, $request);
    $metodo = $configRota['metodo'];
    if(isset($configRota['parametro'])){
        $objeto->$metodo($configRota['parametro']);
    }
    else {
        $objeto->$metodo();
    }
} catch (ResourceNotFoundException $ex) {
    $objeto = new ControllerIndex($response, $twig, $sessao, $request);
    $objeto->erro();
}
$response->send();