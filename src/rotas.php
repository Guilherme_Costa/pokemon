<?php

namespace POKEMON;

use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

$rotas = new RouteCollection();

//Index
$rotas->add('index', new Route('/', [
    'controlador'=>'\POKEMON\controller\ControllerIndex',
    'metodo'=>'show']));
$rotas->add('noticia', new Route('/noticia', [
    'controlador'=>'\POKEMON\controller\ControllerIndex',
    'metodo'=>'noticia']));

//Download
$rotas->add('download', new Route('/download', [
    'controlador'=>'\POKEMON\controller\ControllerDownload',
    'metodo'=>'show']));

//Usuario
$rotas->add('user', new Route('/user', [
    'controlador'=>'\POKEMON\controller\ControllerUser',
    'metodo'=>'show']));
$rotas->add('register', new Route('/register', [
    'controlador'=>'\POKEMON\controller\ControllerUser',
    'metodo'=>'cadastrar']));
$rotas->add('login', new Route('/login', [
    'controlador'=>'\POKEMON\controller\ControllerUser',
    'metodo'=>'entrar']));
$rotas->add('logout', new Route('/logout', [
    'controlador'=>'\POKEMON\controller\ControllerUser',
    'metodo'=>'sair']));
$rotas->add('excluir', new Route('/excluir', [
    'controlador'=>'\POKEMON\controller\ControllerUser',
    'metodo'=>'excluir']));
    
//Comunidade 
$rotas->add('community', new Route('/community', [
    'controlador'=>'\POKEMON\controller\ControllerCommunity',
    'metodo'=>'show']));
$rotas->add('publication', new Route('/publication', [
    'controlador'=>'\POKEMON\controller\ControllerCommunity',
    'metodo'=>'publicar']));
$rotas->add('comment', new Route('/comment', [
    'controlador'=>'\POKEMON\controller\ControllerCommunity',
    'metodo'=>'comentar']));
$rotas->add('delete_p', new Route('/delete_p', [
    'controlador'=>'\POKEMON\controller\ControllerCommunity',
    'metodo'=>'delete_p']));
$rotas->add('delete_n', new Route('/delete_n', [
    'controlador'=>'\POKEMON\controller\ControllerCommunity',
    'metodo'=>'delete_n']));
$rotas->add('alterar', new Route('/alterar', [
    'controlador'=>'\POKEMON\controller\ControllerCommunity',
    'metodo'=>'alterar']));
return $rotas;

