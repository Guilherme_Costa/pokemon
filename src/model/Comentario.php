<?php

namespace POKEMON\model;

use POKEMON\util\Conexao;
use PDO;
use POKEMON\entity\Comentarios;

class Comentario{

    public function inserir(Comentarios $comentario){
        try {
            $sql = 'INSERT INTO comentario (conteudo, id_cliente, id_publicacao, data) VALUES (:conteudo, :id_c, :id_p, :data)';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(':conteudo', $comentario->getConteudo());
            $p_sql->bindValue(':id_c', $comentario->getId_c());
            $p_sql->bindValue(':id_p', $comentario->getId_p());
            $p_sql->bindValue(':data', $comentario->getData());
            $p_sql->execute();
            $p_sql->setFetchMode(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            print_r('Deu ruim de buscar no banco');
        }
        
    }

    public function listarComentario(){
        try {
            $sql = 'SELECT * FROM comentario';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->execute();
            $p_sql->setFetchMode(PDO::FETCH_OBJ);
            return $p_sql->fetchAll();
        } catch (Exception $ex) {
            print_r('Deu ruim de buscar no banco');
        }
    }

    public function delete_post($id){
        try {
            $sql = 'DELETE FROM comentario WHERE id_publicacao = :id';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(':id', $id);
            $p_sql->execute();
            $p_sql->setFetchMode(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            print_r('Deu ruim de buscar no banco');
        }
    }

    public function delete_um($id){
        try {
            $sql = 'DELETE FROM comentario WHERE id = :id';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(':id', $id);
            $p_sql->execute();
            $p_sql->setFetchMode(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            print_r('Deu ruim de buscar no banco');
        }
    }
    
}
