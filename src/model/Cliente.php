<?php

namespace POKEMON\model;

use POKEMON\util\Conexao;
use PDO;
use POKEMON\entity\Clientes;

class Cliente{

    public function verificar_c($email, $nome){
        try {
            $sql = 'SELECT * FROM cliente WHERE email = :email OR nome = :nome';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(':email', $email);
            $p_sql->bindValue(':nome', $nome);
            $p_sql->execute();
            $p_sql->setFetchMode(PDO::FETCH_OBJ);
            return $p_sql->fetch();
        } catch (Exception $ex) {
            print_r('Deu ruim de buscar no banco');
        }
    }

    public function inserir(Clientes $cliente){
        try {
            $sql = 'INSERT INTO cliente (nome, email, senha, status, data) VALUES (:nome, :email, :senha, 1, :data)';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(':nome', $cliente->getNome());
            $p_sql->bindValue(':email', $cliente->getEmail());
            $p_sql->bindValue(':senha', $cliente->getSenha());
            $p_sql->bindValue(':data', $cliente->getData());
            $p_sql->execute();
            $p_sql->setFetchMode(PDO::FETCH_OBJ);
            //return $p_sql->fetch();
        } catch (Exception $ex) {
            print_r('Deu ruim de buscar no banco');
        }
        
    }

    public function verificar_l($nome, $senha){
        try {
            $sql = 'SELECT * FROM cliente WHERE nome = binary :nome AND senha = :senha AND status = 1';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(':nome', $nome);
            $p_sql->bindValue(':senha', $senha);
            $p_sql->execute();
            $p_sql->setFetchMode(PDO::FETCH_OBJ);
            return $p_sql->fetch();
        } catch (Exception $ex) {
            print_r('Deu ruim de buscar no banco');
        }
    }

    public function retorna_id($nome, $senha) {
        try {
            $sql = 'SELECT * FROM cliente WHERE nome = :nome AND senha = :senha';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(':nome', $nome);
            $p_sql->bindValue(':senha', $senha);
            $p_sql->execute();

            //$rows = $p_sql->rowCount();
            $row = $p_sql->fetch(PDO::FETCH_SERIALIZE);

            return $row['id'];

            } catch (Exception $ex) {
                print_r('Deu ruim de buscar no banco');
            }
    }

    public function retorna_adm() {
        try {
            $sql = 'SELECT * FROM cliente WHERE nome = "admin"';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->execute();
            $row = $p_sql->fetch(PDO::FETCH_SERIALIZE);
            return $row['id'];
            } catch (Exception $ex) {
                print_r('Deu ruim de buscar no banco');
            }
    }

    public function listarCliente(){
        try {
            $sql = 'SELECT * FROM cliente';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->execute();
            $p_sql->setFetchMode(PDO::FETCH_OBJ);
            return $p_sql->fetchAll();
        } catch (Exception $ex) {
            print_r('Deu ruim de buscar no banco');
        }
    }

    public function excluir($id, $nome){
        try {
            $sql = 'UPDATE cliente SET status = 0 WHERE id = :id AND nome = :nome';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(':nome', $nome);
            $p_sql->bindValue(':id', $id);
            $p_sql->execute();
        } catch (Exception $ex) {
            print_r('Deu ruim de buscar no banco');
        }
    }
}
