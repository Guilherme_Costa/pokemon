<?php

namespace POKEMON\model;

use POKEMON\util\Conexao;
use PDO;
use POKEMON\entity\Publicacoes;

class Publicacao{

    public function inserir(Publicacoes $publicacao){
        try {
            $sql = 'INSERT INTO publicacao (titulo, conteudo, id_cliente, data) VALUES (:titulo, :conteudo, :id_c, :data)';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(':titulo', $publicacao->getTitulo());
            $p_sql->bindValue(':conteudo', $publicacao->getConteudo());
            $p_sql->bindValue(':id_c', $publicacao->getId_c());
            $p_sql->bindValue(':data', $publicacao->getData());
            $p_sql->execute();
            $p_sql->setFetchMode(PDO::FETCH_OBJ);
            //return $p_sql->fetch();
        } catch (Exception $ex) {
            print_r('Deu ruim de buscar no banco');
        }
        
    }

    public function listarPublicacao(){
        try {
            $sql = 'SELECT * FROM publicacao ORDER BY id DESC';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->execute();
            $p_sql->setFetchMode(PDO::FETCH_OBJ);
            return $p_sql->fetchAll();
        } catch (Exception $ex) {
            print_r('Deu ruim de buscar no banco');
        }
    }

    public function inserir_n(Publicacoes $publicacao){
        try {
            $sql = 'INSERT INTO noticia (titulo, conteudo, id_cliente, data) VALUES (:titulo, :conteudo, :id_c, :data)';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(':titulo', $publicacao->getTitulo());
            $p_sql->bindValue(':conteudo', $publicacao->getConteudo());
            $p_sql->bindValue(':id_c', $publicacao->getId_c());
            $p_sql->bindValue(':data', $publicacao->getData());
            $p_sql->execute();
            $p_sql->setFetchMode(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            print_r('Deu ruim de buscar no banco');
        }
        
    }

    public function listarNoticia(){
        try {
            $sql = 'SELECT * FROM noticia ORDER BY id DESC';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->execute();
            $p_sql->setFetchMode(PDO::FETCH_OBJ);
            return $p_sql->fetchAll();
        } catch (Exception $ex) {
            print_r('Deu ruim de buscar no banco');
        }
    }

    public function delete_p($id){
        try {
            $sql = 'DELETE FROM publicacao WHERE id = :id';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(':id', $id);
            $p_sql->execute();
            $p_sql->setFetchMode(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            print_r('Deu ruim de buscar no banco');
        }
    }

    public function delete_n($id){
        try {
            $sql = 'DELETE FROM noticia WHERE id = :id';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(':id', $id);
            $p_sql->execute();
            $p_sql->setFetchMode(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            print_r('Deu ruim de buscar no banco');
        }
    }
    
}
