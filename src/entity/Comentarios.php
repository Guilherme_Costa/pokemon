<?php

namespace POKEMON\entity;

class Comentarios{
    private $conteudo;
    private $id_c;
    private $id_p;
    private $data;
    
    function __construct() {

    }

    function getConteudo() {
        return $this->conteudo;
    }

    function getId_c() {
        return $this->id_c;
    }

    function getId_p() {
        return $this->id_p;
    }

    function getData(){
        return $this->data;
    }

    function setConteudo($conteudo){
        $this->conteudo = $conteudo;
    }

    function setId_c($id_c){
        $this->id_c = $id_c;
    }

    function setId_p($id_p){
        $this->id_p = $id_p;
    }

    function setData($data){
        $this->data = $data;
    }
}