<?php

namespace POKEMON\entity;

class Clientes{
    private $nome;
    private $email;
    private $senha;
    private $data;
    
    function __construct() {

    }

    function getNome() {
        return $this->nome;
    }

    function getEmail() {
        return $this->email;
    }

    function getSenha() {
        return $this->senha;
    }

    function getData(){
        return $this->data;
    }

    function setNome($nome){
        $this->nome = $nome;
    }

    function setEmail($email){
        $this->email = $email;
    }

    function setSenha($senha){
        $this->senha = $senha;
    }

    function setData($data){
        $this->data = $data;
    }
}