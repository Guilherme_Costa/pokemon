<?php

namespace POKEMON\entity;

class Publicacoes{
    private $titulo;
    private $conteudo;
    private $id_c;
    private $data;
    
    function __construct() {

    }

    function getTitulo() {
        return $this->titulo;
    }

    function getConteudo() {
        return $this->conteudo;
    }

    function getId_c() {
        return $this->id_c;
    }

    function getData(){
        return $this->data;
    }

    function setTitulo($titulo){
        $this->titulo = $titulo;
    }

    function setConteudo($conteudo){
        $this->conteudo = $conteudo;
    }

    function setId_c($id_c){
        $this->id_c = $id_c;
    }

    function setData($data){
        $this->data = $data;
    }
}